<?php
require_once('recursos/estatico/librerias/conectorpump.php');
require_once('sistema/configuracion.php');
$oauth_token = $_GET['oauth_token'];
$oauth_verifier = $_GET['oauth_verifier'];
#comprobamos si están las credenciales de acceso permanente.
if(!$oauth_token || !$oauth_verifier) {
	echo "no hay token";
	exit;
}
else {
	#buscar las credenciales temporales
	$query = mysql_query("select user, token_secret from token where token='$oauth_token'", $con);
	if(!$query) {
		echo "Hubo un error al solicitar información a la base de datos: <br>".mysql_errno()." - ".mysql_error();
		exit;
	}
	$credenciales = mysql_fetch_array($query);
	$user = $credenciales['user'];
	$oauth_token_secret = $credenciales['token_secret'];
	$host = ConectorPump::extractorPumpid($user, "nodo");
	#buscar las credenciales del cliente
	$query = mysql_query("select consumer_key, consumer_secret, conexion from oauth where servidor='$host'", $con);
	if(!$query) {
		echo "Hubo un error al solicitar información a la base de datos: <br>".mysql_errno()." - ".mysql_error();
		exit;
	}
	$credenciales_sitio = mysql_fetch_array($query);
	$conexion = $credenciales_sitio['conexion'];
	$oauth_consumer_key = $credenciales_sitio['consumer_key'];
	$oauth_consumer_secret = $credenciales_sitio['consumer_secret'];
	#solicitar token acceso
	$cred = ConectorPump::solicitarTokenacc($oauth_consumer_key, $oauth_consumer_secret, $oauth_token, $oauth_token_secret, $oauth_verifier, $conexion, $host, $cliente);
	if($cred['oauth_token']) {
		$token = $cred['oauth_token'];
		$token_secret = $cred['oauth_token_secret'];
		$sql = "UPDATE token SET token='$token', token_secret='$token_secret', acceso='1' ";
		$sql.= "WHERE user='$user'";
		$registrar_credenciales = mysql_query($sql, $con);
		if(!$registrar_credenciales) {
			echo "Hubo un error al solicitar información a la base de datos: <br>".mysql_errno()." - ".mysql_error();
			exit;
		}
	}	
	else {
		echo "Hubo un error al solicitar las credenciales, el sitio devolvió una respuesta no valida:<br>";
		echo "<pre>";
		print_r($cred);
		echo "<pre>";
		exit;
	}
	#Guardamos las cookies con los datos de la sesión.
	$segundos = ConectorPump::selloTiempo();
	$expire = $segundos+2592000;
	$hash_token = hash("SHA256", $token);
	$uc = setcookie("usuario", $user, $expire);
	$tc = setcookie("hash_token", $hash_token, $expire);
	if(!$uc || !$tc) {
		echo "Habilite el uso de cookies en este sitio, de lo contrario no podrá utilizar el servicio.";
		exit;
	}
	else {
		header('Location: index.php');
	}
}
?>