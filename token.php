<?php
#Se conecta con la base de datos que tiene las credenciales del cliente
require('sistema/configuracion.php');
require_once('recursos/estatico/librerias/conectorpump.php');
$pump_id = $_POST['pump_id'];
#validamos la dirección de cuenta de pump.io
if(!filter_var($pump_id, FILTER_VALIDATE_EMAIL)) { echo "Tu dirección es invalida."; exit; }
else {
	#obtenemos el nodo del usuario
	$host = ConectorPump::extractorPumpid($pump_id, "nodo");
	#llamamos a la base de datos para encontrar las credenciales de dicho servidor
	$query = mysql_query("select consumer_key, consumer_secret from oauth where servidor='$host'", $con);
	if(!$query) {
		echo "Hubo un error al solicitar información a la base de datos: <br>".mysql_errno()." - ".mysql_error();
		exit;
	}
	$coincidencias=mysql_num_rows($query);
	#Si las credenciales no se encuentran se obtendrán
	if($coincidencias==0) 
	{
		$cred = ConectorPump::registrarCliente($host, $cliente);
		if($cred['consumer_key']) {
			$oauth_consumer_key = $cred['consumer_key'];
			$oauth_consumer_secret = $cred['consumer_secret'];
			$conexion = $cred['conexion'];
			$sql = "INSERT INTO oauth (servidor, consumer_key, consumer_secret, conexion) ";
			$sql.= "VALUES ('$host','$oauth_consumer_key','$oauth_consumer_secret','$conexion')";
			$registrar_credenciales = mysql_query($sql, $con);
			if(!$registrar_credenciales) {
				echo mysql_errno()." - ".mysql_error();
				exit;
			}
		}
		else {
			echo "Hubo un error al solicitar las credenciales, el sitio devolvió una respuesta no valida:<br>";
			echo "<pre>";
			print_r($cred);
			echo "<pre>";
			exit;
		}
	}
	#borramos el token anterior si es que existe, así se pedirá autorización cada vez que el usuario lo quiera.
	$query = mysql_query("SELECT id FROM token WHERE user='$pump_id'", $con);
	if(!$query) {
		echo "Hubo un error al solicitar información a la base de datos: <br>".mysql_errno()." - ".mysql_error();
		exit;
	}
	$del = mysql_num_rows($query);
	if($del) {
		$eliminar = mysql_query("DELETE FROM token WHERE user='$pump_id'", $con);
		if(!$eliminar) {
			echo "Hubo un error al solicitar información a la base de datos: <br>".mysql_errno()." - ".mysql_error();
			exit;
		}	
	}
	$query = mysql_query("select consumer_key, consumer_secret, conexion from oauth where servidor='$host'", $con);
	if(!$query) {
		echo "Hubo un error al solicitar información a la base de datos: <br>".mysql_errno()." - ".mysql_error();
		exit;
	}
	$credenciales = mysql_fetch_array($query);
	$oauth_consumer_key = $credenciales['consumer_key'];
	$oauth_consumer_secret = $credenciales['consumer_secret'];
	$conexion = $credenciales['conexion'];
	$token_tmp = ConectorPump::solicitarTokentmp($oauth_consumer_key, $oauth_consumer_secret, $conexion, $host, $cliente);
	#Comprobamos que la respuesta sea las credenciales temporales
	if($token_tmp['oauth_token']) {
		$token = $token_tmp['oauth_token'];
		$token_secret = $token_tmp['oauth_token_secret'];
		$query = "INSERT INTO token (user, token, token_secret) ";
		$query.= "VALUES ('$pump_id','$token','$token_secret')";
		$query = mysql_query($query, $con);
		if(!$query) {
			echo "Hubo un error al solicitar información a la base de datos: <br>".mysql_errno()." - ".mysql_error();
			exit;
		}
		else {
			$url_auth = 'http'.$conexion.'://'.$host.'/oauth/authorize?oauth_token='.$token;
			echo "Si su navegador no lo redirecciona, haga clic aquí: <a href='".$url_auth."'>".$url_auth."</a>";
			header('Location: '.$url_auth);
		}
	}
	else {
		echo "Hubo un error al solicitar las credenciales, el sitio devolvió una respuesta no valida:<br>";
		echo "<pre>";
		print_r($token_tmp);
		echo "<pre>";
		exit;
	}
}
?>